package com.cristinaffmartins.medicationapp.utils

import android.content.Context
import com.cristinaffmartins.medicationapp.model.MedicationRepository
import com.cristinaffmartins.medicationapp.model.local.MedicationDatabase
import com.cristinaffmartins.medicationapp.model.network.MockApiService
import com.cristinaffmartins.medicationapp.viewmodel.MedicationViewModelFactory

class MedicationModule(private val context: Context) {
    fun getMedicationViewModelFactory() = MedicationViewModelFactory(getMedicationRepository())

    private fun getMedicationRepository() = MedicationRepository(
        getApiService(),
        MedicationDatabase.getInstance(context).medicationDao()
    )

    // Replace here with real implementation of ApiService
    private fun getApiService() = MockApiService(context)
}