package com.cristinaffmartins.medicationapp.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cristinaffmartins.medicationapp.R
import com.cristinaffmartins.medicationapp.model.local.MedicationEntity
import kotlinx.android.synthetic.main.medication_list_item.view.*

class MedicationListAdapter(private val context: Context)
    : RecyclerView.Adapter<MedicationListAdapter.MedicationViewHolder>() {

    private var medicationList = emptyList<MedicationEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicationViewHolder {
        return MedicationViewHolder(LayoutInflater.from(context).inflate(R.layout.medication_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return medicationList.size
    }

    override fun onBindViewHolder(holder: MedicationViewHolder, position: Int) {
        with(medicationList[position]) {
            holder.medicationNameTextView.text = name
            holder.medicationQuantityTextView.text = context.getString(R.string.quantity, quantity)
        }
    }

    fun setMedicationList(medications: List<MedicationEntity>) {
        medicationList = medications
        notifyDataSetChanged()
    }

    class MedicationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val medicationNameTextView: TextView = itemView.medication_name
        val medicationQuantityTextView: TextView = itemView.quantity
    }
}