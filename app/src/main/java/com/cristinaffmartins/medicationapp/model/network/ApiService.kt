package com.cristinaffmartins.medicationapp.model.network

import io.reactivex.Single

interface ApiService {
    fun getMedicationList() : Single<List<Medication>>
}