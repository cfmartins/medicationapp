package com.cristinaffmartins.medicationapp.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Single

@Dao
interface MedicationDao {
    @Insert
    fun addMedications(medications: List<MedicationEntity>)

    @Insert
    fun addMedication(medication: MedicationEntity)

    @Query("SELECT * FROM medications")
    fun getMedications() : Single<List<MedicationEntity>>
}