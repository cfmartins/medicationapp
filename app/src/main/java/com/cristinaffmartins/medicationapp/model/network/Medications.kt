package com.cristinaffmartins.medicationapp.model.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Medications(
    @Json(name = "medications")
    val medications: List<Medication>
)