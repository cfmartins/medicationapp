package com.cristinaffmartins.medicationapp.model.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Medication(
    @Json(name = "name")
    val name: String,
    @Json(name = "quantity")
    val quantity: Int
)