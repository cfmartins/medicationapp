package com.cristinaffmartins.medicationapp.model

import com.cristinaffmartins.medicationapp.model.local.MedicationDao
import com.cristinaffmartins.medicationapp.model.local.MedicationEntity
import com.cristinaffmartins.medicationapp.model.network.ApiService
import com.cristinaffmartins.medicationapp.model.network.Medication
import io.reactivex.Completable
import io.reactivex.Single

class MedicationRepository(private val apiService: ApiService, private val medicationDao: MedicationDao) {
    fun getMedications() : Single<List<MedicationEntity>> =
        medicationDao.getMedications()
            .flatMap {
                // Database is the source of truth: if empty we get data from network and then save it to database
                if (it.isEmpty()) {
                    getMedicationsFromNetworkAndSave()
                } else {
                    Single.just(it)
                }
            }

    fun addMedication(medication: MedicationEntity) =
        Completable.fromAction { medicationDao.addMedication(medication) }

    private fun getMedicationsFromNetworkAndSave(): Single<List<MedicationEntity>> {
        return apiService.getMedicationList()
            .map { mapToMedicationEntityList(it) }
            .doOnSuccess { medicationDao.addMedications(it) }
    }

    private fun mapToMedicationEntityList(medicationList: List<Medication>) : List<MedicationEntity> {
        val medicationEntityList = arrayListOf<MedicationEntity>()
        medicationList.forEach {
            medicationEntityList.add(MedicationEntity(it.name, it.quantity))
        }
        return medicationEntityList
    }
}
