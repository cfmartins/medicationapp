package com.cristinaffmartins.medicationapp.model.network

import android.content.Context
import com.squareup.moshi.Moshi
import io.reactivex.Single
import java.io.IOException

class MockApiService(private val context: Context) :
    ApiService {

    override fun getMedicationList(): Single<List<Medication>> = Single.just(
        readJSONFromAsset()?.let {
            Moshi.Builder()
                .build()
                .adapter(Medications::class.java)
                .fromJson(it)?.medications ?: emptyList()
        } ?: emptyList()
    )

    private fun readJSONFromAsset() =
        try {
            context.assets.open(MEDICATION_JSON_FILE).bufferedReader().use {
                it.readText()
            }
        } catch (exception: IOException) {
            null
        }

    companion object {
        private const val MEDICATION_JSON_FILE = "medications.json"
    }
}