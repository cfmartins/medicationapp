package com.cristinaffmartins.medicationapp.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [MedicationEntity::class],
    version = 1)
abstract class MedicationDatabase : RoomDatabase() {

    abstract fun medicationDao() : MedicationDao

    companion object {

        @Volatile
        private var instance: MedicationDatabase? = null

        fun getInstance(context: Context): MedicationDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private const val DATABASE_NAME: String = "medication-database"

        private fun buildDatabase(context: Context): MedicationDatabase {
            return Room.databaseBuilder(context, MedicationDatabase::class.java, DATABASE_NAME)
                .build()
        }
    }
}