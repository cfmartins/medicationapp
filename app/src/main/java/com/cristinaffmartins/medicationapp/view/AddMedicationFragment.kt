package com.cristinaffmartins.medicationapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.cristinaffmartins.medicationapp.R
import com.cristinaffmartins.medicationapp.utils.MedicationModule
import com.cristinaffmartins.medicationapp.utils.getTextInput
import com.cristinaffmartins.medicationapp.utils.hideKeyboard
import com.cristinaffmartins.medicationapp.viewmodel.AddMedicationViewModel
import kotlinx.android.synthetic.main.fragment_add_medication.*

class AddMedicationFragment : Fragment() {

    private val addMedicationViewModel by viewModels<AddMedicationViewModel> {
        MedicationModule(requireContext()).getMedicationViewModelFactory()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_medication, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpSaveButton()
        observeSaveState()
        observeFieldErrors()
        resetFieldErrorsOnInput()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(CURRENT_NAME, medication_name_input.getTextInput())
        outState.putString(CURRENT_QUANTITY, medication_quantity_input.getTextInput())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            medication_name_input.editText?.setText(savedInstanceState.getString(CURRENT_NAME, DEFAULT_STATE))
            medication_quantity_input.editText?.setText(savedInstanceState.getString(CURRENT_QUANTITY, DEFAULT_STATE))
        }
    }

    private fun setUpSaveButton() {
        save_medication_button.setOnClickListener {
            addMedicationViewModel.addMedication(medication_name_input.getTextInput(), medication_quantity_input.getTextInput())
        }
    }

    private fun observeSaveState() {
        addMedicationViewModel.medicationSavedLiveData.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                navigateBackToMedicationList()
            } else {
                Toast.makeText(requireContext(), getString(R.string.save_error_message), LENGTH_LONG).show()
            }
        })
    }

    private fun navigateBackToMedicationList() {
        findNavController().navigate(R.id.action_addMedicationFragment_to_medicationListFragment)
    }

    private fun observeFieldErrors() {
        addMedicationViewModel.showFieldsErrorLiveData.observe(viewLifecycleOwner, Observer {
            if (it.first) medication_name_input.error = getString(R.string.empty_name_message)
            if (it.second) medication_quantity_input.error = getString(R.string.empty_quantity_message)
        })
    }

    private fun resetFieldErrorsOnInput() {
        medication_name_input.editText?.addTextChangedListener(
            onTextChanged = { _, _, _, _ -> medication_name_input.error = null }
        )
        medication_quantity_input.editText?.addTextChangedListener(
            onTextChanged = { _, _, _, _ -> medication_quantity_input.error = null }
        )
    }

    companion object {
        private const val CURRENT_NAME = "currentName"
        private const val CURRENT_QUANTITY = "currentQuantity"
        private const val DEFAULT_STATE = ""
    }
}