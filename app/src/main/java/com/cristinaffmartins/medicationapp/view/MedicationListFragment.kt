package com.cristinaffmartins.medicationapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.cristinaffmartins.medicationapp.R
import com.cristinaffmartins.medicationapp.utils.MedicationListAdapter
import com.cristinaffmartins.medicationapp.utils.MedicationModule
import com.cristinaffmartins.medicationapp.utils.hide
import com.cristinaffmartins.medicationapp.utils.show
import com.cristinaffmartins.medicationapp.viewmodel.MedicationListViewModel
import kotlinx.android.synthetic.main.fragment_medication_list.*

class MedicationListFragment : Fragment() {

    private val medicationListAdapter : MedicationListAdapter by lazy {
        MedicationListAdapter(requireContext())
    }

    private val medicationListViewModel by viewModels<MedicationListViewModel> {
        MedicationModule(requireContext()).getMedicationViewModelFactory()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_medication_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setUpAddButton()
        observeMedicationList()
        observeEmptyState()
    }

    private fun setUpRecyclerView() {
        medication_list.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = medicationListAdapter
        }
    }

    private fun setUpAddButton() {
        add_medication_btn.setOnClickListener {
            findNavController().navigate(R.id.action_medicationListFragment_to_addMedicationFragment)
        }
    }

    private fun observeMedicationList() {
        medicationListViewModel.medicationListLiveData.observe(viewLifecycleOwner, Observer {
            medicationListAdapter.setMedicationList(it)
        })
    }

    private fun observeEmptyState() {
        medicationListViewModel.showEmptyStateLiveData.observe(viewLifecycleOwner, Observer {
            if (it) {
                no_medications_message.show()
            } else {
                no_medications_message.hide()
            }
        })
    }
}