package com.cristinaffmartins.medicationapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cristinaffmartins.medicationapp.model.MedicationRepository
import com.cristinaffmartins.medicationapp.model.local.MedicationEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MedicationListViewModel(private val medicationRepository: MedicationRepository) : ViewModel() {
    val medicationListLiveData: LiveData<List<MedicationEntity>>
        get() = medicationList

    val showEmptyStateLiveData: LiveData<Boolean>
        get() = showEmptyState

    private val medicationList = MutableLiveData<List<MedicationEntity>>()
    private val showEmptyState = MutableLiveData<Boolean>()
    private val compositeDisposable = CompositeDisposable()

    init {
        loadMedicationList()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun loadMedicationList() {
        medicationRepository.getMedications()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( { handleSuccess(it) }, { handleError() })
            .also { compositeDisposable.add(it) }
    }

    private fun handleSuccess(medications: List<MedicationEntity>) {
        if (medications.isNotEmpty()) {
            medicationList.value = medications
            showEmptyState.value = false
        } else {
            showEmptyState.value = true
        }
    }

    private fun handleError() {
        showEmptyState.value = true
    }
}