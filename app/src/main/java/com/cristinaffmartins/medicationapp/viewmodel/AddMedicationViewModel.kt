package com.cristinaffmartins.medicationapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cristinaffmartins.medicationapp.model.MedicationRepository
import com.cristinaffmartins.medicationapp.model.local.MedicationEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class AddMedicationViewModel(private val medicationRepository: MedicationRepository) : ViewModel() {

    val showFieldsErrorLiveData: LiveData<Pair<Boolean, Boolean>>
        get() = showFieldsError

    val medicationSavedLiveData: LiveData<Boolean>
        get() = medicationSaved

    private val showFieldsError = MutableLiveData<Pair<Boolean, Boolean>>()
    private val medicationSaved = MutableLiveData<Boolean>()
    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun addMedication(name: String, quantity: String) {
        if (name.isNotEmpty() && quantity.isNotEmpty()) {
            medicationRepository.addMedication(MedicationEntity(name, quantity.toInt()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { medicationSaved.value = true }, { medicationSaved.value = false })
                .also { compositeDisposable.add(it) }
        } else {
            showFieldsError.value = Pair(name.isEmpty(), quantity.isEmpty())
        }
    }
}