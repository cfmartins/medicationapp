package com.cristinaffmartins.medicationapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cristinaffmartins.medicationapp.model.MedicationRepository

class MedicationViewModelFactory(private val medicationRepository: MedicationRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MedicationListViewModel::class.java)) {
            return MedicationListViewModel(medicationRepository) as T
        } else if (modelClass.isAssignableFrom(AddMedicationViewModel::class.java)) {
            return AddMedicationViewModel(medicationRepository) as T
        }
        throw IllegalArgumentException()
    }
}