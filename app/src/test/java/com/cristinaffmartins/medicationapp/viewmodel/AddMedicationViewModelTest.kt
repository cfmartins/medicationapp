package com.cristinaffmartins.medicationapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.cristinaffmartins.medicationapp.model.MedicationRepository
import com.cristinaffmartins.medicationapp.model.local.MedicationEntity
import io.reactivex.Completable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.verify
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class AddMedicationViewModelTest {

    @Mock
    private lateinit var medicationRepository: MedicationRepository

    @Mock
    private lateinit var medicationSavedObserver: Observer<Boolean>

    @Mock
    private lateinit var showFieldsErrorObserver: Observer<Pair<Boolean, Boolean>>

    private lateinit var underTest: AddMedicationViewModel

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        underTest = AddMedicationViewModel(medicationRepository)
    }

    @Test
    fun shouldAddMedicationIfNameAndQuantityNotEmpty() {
        given(medicationRepository.addMedication(MedicationEntity(VALID_NAME, VALID_QUANTITY.toInt())))
            .willReturn(Completable.complete())
        underTest.medicationSavedLiveData.observeForever(medicationSavedObserver)

        underTest.addMedication(VALID_NAME, VALID_QUANTITY)

        verify(medicationSavedObserver).onChanged(true)
    }

    @Test
    fun shouldNotifyViewIfAddMedicationFailed() {
        given(medicationRepository.addMedication(MedicationEntity(VALID_NAME, VALID_QUANTITY.toInt())))
            .willReturn(Completable.error(Throwable()))
        underTest.medicationSavedLiveData.observeForever(medicationSavedObserver)

        underTest.addMedication(VALID_NAME, VALID_QUANTITY)

        verify(medicationSavedObserver).onChanged(false)
    }

    @Test
    fun shouldShowFieldsErrorIfNameIsEmpty() {
        underTest.showFieldsErrorLiveData.observeForever(showFieldsErrorObserver)

        underTest.addMedication(INVALID_NAME, VALID_QUANTITY)

        verify(showFieldsErrorObserver).onChanged(Pair(first = true, second = false))
    }

    @Test
    fun shouldShowFieldsErrorIfQuantityIsEmpty() {
        underTest.showFieldsErrorLiveData.observeForever(showFieldsErrorObserver)

        underTest.addMedication(VALID_NAME, INVALID_QUANTITY)

        verify(showFieldsErrorObserver).onChanged(Pair(first = false, second = true))
    }

    @Test
    fun shouldShowFieldsErrorIfNameAndQuantityAreEmpty() {
        underTest.showFieldsErrorLiveData.observeForever(showFieldsErrorObserver)

        underTest.addMedication(INVALID_NAME, INVALID_QUANTITY)

        verify(showFieldsErrorObserver).onChanged(Pair(first = true, second = true))
    }

    companion object {
        private const val VALID_NAME = "name"
        private const val INVALID_NAME = ""
        private const val VALID_QUANTITY = "10"
        private const val INVALID_QUANTITY = ""
    }
}